#coding=utf-8
from locale import *
import time
import datetime
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from bs4 import BeautifulSoup

file_name = "found_asin_"+datetime.datetime.now().strftime('%Y-%m-%d_%H_%M_%S')+".xls"

baseUrl = "https://www.google.com/search?num=100&q="

#选择国家站
switcher = {
    1: "https://www.amazon.com/",
    2: "https://www.amazon.co.uk/",
    3: "https://www.amazon.co.jp/",
    4: "https://www.amazon.de/",
    5: "https://www.amazon.it/",
    6: "https://www.amazon.es/",
    7: "https://www.amazon.fr/"
}
for item in switcher:
    print(str(item) + ":" + switcher.get(item))

country_index = input("Select country amazon site number:")
site_url = switcher.get(int(country_index))
print("Your selection:" + site_url)

keyWords = input("Input search keywords:")
print("Search keywords is:", keyWords)

pingLunNumInput = input("ping lun number:")
print("Your input ping lun num:" + pingLunNumInput)

setlocale(LC_NUMERIC, 'English_US')

driver = webdriver.Firefox()
driver.get(site_url)

WebDriverWait(driver, 60).until(EC.presence_of_element_located((By.ID, 'twotabsearchtextbox')))
driver.find_element_by_id("twotabsearchtextbox").clear()
WebDriverWait(driver, 60).until(EC.presence_of_element_located((By.ID, 'twotabsearchtextbox')))
driver.find_element_by_id("twotabsearchtextbox").send_keys(keyWords)
driver.find_element_by_xpath('//*[@id="nav-search"]/form/div[2]/div/input').click()

while 1:
    soup = BeautifulSoup(driver.page_source, "html.parser")
    allItems = soup.select('li.s-result-item')
    for item in allItems:
        file = open(file_name, 'a')
        span_name = item.select_one('span[name]')
        if span_name is not None:
            asin_text = span_name.get("name")
            star_ele = span_name.select_one('i.a-icon-star > span')
            if star_ele is not None:
                star_text = star_ele.get_text()
            else:
                star_text = "Null"
            pingLun_ele = span_name.select_one(' + a')
            if pingLun_ele is not None:
                pingLun_text = pingLun_ele.get_text()
                pingLunNum = int(atof(pingLun_text))
                if pingLunNum >= int(pingLunNumInput):
                    print(asin_text + '\t' + star_text + '\t' + pingLun_text + " 写入文件")
                    file.writelines(asin_text + '\t' + star_text + '\t' + pingLun_text + '\n')

                else:
                    print(asin_text + '\t' + star_text + '\t' + pingLun_text)
        file.close()
    WebDriverWait(driver, 240).until(EC.presence_of_element_located((By.ID, 'pagnNextLink')))
    nextPageUrl = driver.find_element_by_id("pagnNextLink").get_attribute("href")
    print(nextPageUrl)
    driver.get(nextPageUrl)

driver.quit()


